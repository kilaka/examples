package kilaka.examples.jetty;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import com.google.common.io.CharStreams;
import com.google.common.io.Resources;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class JettyServerTest
{
	private static JettyServer jettyServer;
	
	@BeforeAll
	public static void setup() throws Exception
	{
		jettyServer = new JettyServer();
		jettyServer.start();
	}
	
	@AfterAll
	public static void cleanup() throws Exception
	{
		jettyServer.stop();
	}
	
	@Test
	public void testIndexHtmlReturnedViaHttpCall() throws URISyntaxException, IOException, InterruptedException
	{
		URL url = new URI("http://localhost:8080/index.html").toURL();
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.connect();
		assertThat(connection.getResponseCode()).isEqualTo(200);
		
		String remote;
		try (InputStream is = connection.getInputStream())
		{
			remote = CharStreams.toString(new InputStreamReader(is, StandardCharsets.UTF_8));
		}
		
		//noinspection UnstableApiUsage
		String local = Resources.toString(getClass().getResource("/web/index.html"), StandardCharsets.UTF_8);
		
		assertThat(remote).isEqualTo(local);
	}
}
