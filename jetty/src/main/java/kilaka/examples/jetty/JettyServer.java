package kilaka.examples.jetty;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.thread.ThreadPool;

public class JettyServer
{
	private Server server;
	
	public void start() throws Exception
	{
		server = new Server((ThreadPool) null);
		ServerConnector connector = new ServerConnector(server);
		connector.setPort(8080);
		server.setConnectors(new Connector[]{connector});
		
		ContextHandler context = new ContextHandler();
		context.setContextPath("/");
		context.setResourceBase(".");
		context.setClassLoader(Thread.currentThread().getContextClassLoader());
		
		ResourceHandler resourceHandler = new ResourceHandler();
		resourceHandler.setDirectoriesListed(true);
		resourceHandler.setBaseResource(Resource.newClassPathResource("web"));

//		ErrorPageErrorHandler errorHandler = new ErrorPageErrorHandler();
//		errorHandler.addErrorPage(404, "web/error.html");
//		context.setErrorHandler(errorHandler);
//
//		context.setHandler(new HandlerList(
//				resourceHandler,
//				new DefaultHandler()));
//
//		server.setHandler(context);
		
		server.setHandler(new HandlerList(
				resourceHandler,
				new DefaultHandler()));
		
		server.start();
		
		// Why is this needed???
//		server.join();
	}
	
	public void stop() throws Exception
	{
		server.stop();
	}
	
	public static void main(String[] args) throws Exception
	{
		new JettyServer().start();
	}
}