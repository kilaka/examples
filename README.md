# Examples #

### What is this repository for? ###

Examples for some hello worlds integrating and checking out technologies.

### How do I get set up? ###

clone it and run mvn clean install on the example you're interested in

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* kilaka*at*gmail*dot*com
* Other community or team contact